## Gradle Artifactory Plugin Version 4 Examples
Sample projects that use the Gradle Artifactory Plugin.
The plugin adds the "artifactoryPublish" task.
The Plugin documenation is available at:
https://www.jfrog.com/confluence/display/RTF/Gradle+Artifactory+Plugin

#### Running the examples
```console
> gradle artifactoryPublish

or with the gradle wrapper in Unix

> ./gradlew artifactoryPublish

and the gradle wrapper in Windows

> gradlew.bat artifactoryPublish
```

### gradle-example
Sample project that uses the Gradle Artifactory Plugin with Gradle Publications.

# Deploy to Artifactory

for deploying from Bitbucket Pipelines to an instance of Artifactory.

## How to use it

This example uses the Gradle Artifactory Plugin for artifacts and build-info deployment to Artifactory:

* Add required environment variables to your Bitbucket enviroment variables.
* Edit the `build.gradle` file in your repository to add the `artifactory-gradle-plugin`. If you copy/paste from this example, make sure to set the value of the *contextUrl* with your Artifactory URL, as well as the other Artifactory properties. For more configuration information see the .



## Required environment variables

* **`ARTIFACTORY_CONTEXT_URL`**: (Required) URL for Artifactory

## Deploy to Artifactory

```
> gradle artifactoryPublish
```

## Build configuration

```
# You can use a Docker image from Docker Hub or your own container
# registry for your build environment.
image: niaquinto/gradle:2.13
pipelines:
  default:
    - step:
        script: # Modify the commands below to build your repository.
          - gradle artifactoryPublish

```


* Builds and deploys the JAR file to Artifactory.


